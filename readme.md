# Liima Client

This is a Java client Library to simplify interacting with [Liima](http://www.liima.org/) - an open source 
integration and deployment middleware available at [GitHub](https://github.com/liimaorg/).

The client is the base for the Liima Maven Plugin we use to deploy applications or promote them between stages as well 
as the client for the Mobiliar Build Monitor (Mobitor).

Its current capabilities are:

* Create Deployment Request to Liima
* Track a running deployment (via polling and configurable timeout)
* retrieve version and deployment information of a server / application
* retrieve information on Liima resources (Servers, Applications, Meta-Data)


## Requirements

The current client implementation supports only client certificate authentication.

The certificate must be passed via Java properties:

```bash
-Djavax.net.ssl.keyStore=/path/to/certificate/certificate.pfx 
-Djavax.net.ssl.keyStorePassword=certificatepassword 
-Djavax.net.ssl.keyStoreType=PKCS12
```

for example by setting these values in the environment variable [JAVA_TOOL_OPTIONS](https://docs.oracle.com/javase/8/docs/platform/jvmti/jvmti.html#tooloptions).


## Artifacts

A release is first deployed into [Sonatype OSS Releases](https://oss.sonatype.org/content/repositories/releases/ch/mobi/liima/client/liima-client/) and 
then automatically synced to [maven central](https://repo1.maven.org/maven2/ch/mobi/liima/client/liima-client/) within 10-15 minutes.


## Release

To release the Liima client these environment variables are required:

 - GPG_PASSPHRASE (Passphrase to use for the given key)
 - GPG_KEY (name of the gpg key: 35DC9EC5671F05FD)
 - OPENSSL_PASSWORD (password to de-crypt the gpg key)
 - OSS_SONATYPE_JIRA_PASSWORD
 - OSS_SONATYPE_JIRA_USERNAME
