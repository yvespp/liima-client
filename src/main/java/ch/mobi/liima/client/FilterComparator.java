package ch.mobi.liima.client;

/*-
 * §
 * Liima Client
 * --
 * Copyright (C) 2017 - 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonProperty;

public enum FilterComparator {
/*
    <select class="form-control ng-valid ng-dirty ng-touched" id="selectFilterComp">
        <!---->
        <option value="0: lt">&lt;</option>
        <option value="1: lte">&lt;=</option>
        <option value="2: eq">is</option>
        <option value="3: gte">&gt;=</option>
        <option value="4: gt">&gt;</option>
        <option value="5: neq">is not</option>
    </select>

*/
    @JsonProperty("eq") EQ
}
