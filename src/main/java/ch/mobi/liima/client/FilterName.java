package ch.mobi.liima.client;

/*-
 * §
 * Liima Client
 * --
 * Copyright (C) 2017 - 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonProperty;

public enum FilterName {
/*
    <select class="form-control ng-pristine ng-valid ng-touched" id="selectFilterType">
        <!---->
        <option value="0: Object">Application</option>
        <option value="1: Object">Application server</option>
        <option value="2: Object">Build success</option>
        <option value="3: Object">Canceled by</option>
        <option value="4: Object">Canceled on</option>
        <option value="5: Object">Confirmed</option>
        <option value="6: Object">Confirmed by</option>
        <option value="7: Object">Confirmed on</option>
        <option value="8: Object">Created on</option>
        <option value="9: Object">Deployment date</option>
        <option value="10: Object">Deployment parameter</option>
        <option value="11: Object">Deployment parameter value</option>
        <option value="12: Object">Environment</option>
        <option value="13: Object">Id</option>
        <option value="14: Object">Latest deployment job for App Server and Env</option>
        <option value="15: Object">Message</option>
        <option value="16: Object">Reason</option>
        <option value="17: Object">Release</option>
        <option value="18: Object">Requested by</option>
        <option value="19: Object">State</option>
        <option value="20: Object">Targetplatform</option>
        <option value="21: Object">Tracking Id</option>
    </select>
*/

    @JsonProperty("Application") APPLICATION,
    @JsonProperty("Application server") APPLICATION_SERVER,
    @JsonProperty("Environment") ENVIRONMENT,
    @JsonProperty("Latest deployment job for App Server and Env") LATEST_ONLY,
    @JsonProperty("State") STATE,
    @JsonProperty("Tracking Id") TRACKING_ID
}
