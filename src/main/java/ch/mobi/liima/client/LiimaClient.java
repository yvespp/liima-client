package ch.mobi.liima.client;

/*-
 * §
 * Liima Client
 * --
 * Copyright (C) 2017 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.liima.client.dto.AppWithVersion;
import ch.mobi.liima.client.dto.Deployment;
import ch.mobi.liima.client.dto.DeploymentState;
import ch.mobi.liima.client.dto.Environment;
import ch.mobi.liima.client.dto.Property;
import ch.mobi.liima.client.dto.Release;
import ch.mobi.liima.client.dto.Resource;
import ch.mobi.liima.client.dto.ResourceRelation;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static java.text.MessageFormat.format;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;

public class LiimaClient {

    private static final Logger LOG = LoggerFactory.getLogger(LiimaClient.class);

    private final LiimaConfiguration liimaConfiguration;

    private long maxPollingDurationInMs = 600000;
    private long pollingIntervalInMs = 2000;

    public LiimaClient(@NotNull LiimaConfiguration liimaConfiguration) {
        this.liimaConfiguration = liimaConfiguration;
    }

    public CloseableHttpClient createHttpClient() {
        int timeout = liimaConfiguration.getTimeout();
        RequestConfig requestConfig = RequestConfig.custom()
                                                   .setConnectTimeout(timeout * 1000)
                                                   .setConnectionRequestTimeout(timeout * 1000)
                                                   .setSocketTimeout(timeout * 1000)
                                                   .build();

        SSLContext sslContext = SSLContexts.createSystemDefault();
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext);

        Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder
                .<ConnectionSocketFactory> create()
                .register("https", sslsf)
                .register("http", PlainConnectionSocketFactory.INSTANCE)
                .build();

        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
        cm.setMaxTotal(liimaConfiguration.getMaxConnections());
        cm.setDefaultMaxPerRoute(liimaConfiguration.getMaxConnections());

        return HttpClientBuilder.create()
                                .useSystemProperties()
                                .setDefaultRequestConfig(requestConfig)
                                .setConnectionManager(cm)
                                .build();
    }

    public long getMaxPollingDurationInMs() {
        return maxPollingDurationInMs;
    }

    public void setMaxPollingDurationInMs(long maxPollingDurationInMs) {
        this.maxPollingDurationInMs = maxPollingDurationInMs;
    }

    public long getPollingIntervalInMs() {
        return pollingIntervalInMs;
    }

    public void setPollingIntervalInMs(long pollingIntervalInMs) {
        this.pollingIntervalInMs = pollingIntervalInMs;
    }

    @Nullable
    public Deployment retrieveDeployment(@NotNull String serverName, @NotNull String environment) {
        List<Deployment> deployments = retrieveDeployments(null, singletonList(serverName), null, singletonList(environment), true);
        return onlyDeploymentOrNull(deployments);
    }

    @Nullable
    public List<Deployment> retrieveDeployments(@NotNull String serverName) {
        return retrieveDeployments(null, singletonList(serverName), null, null, true);
    }

    @Nullable
    public List<Deployment> retrieveDeployments(DeploymentState deploymentState, List<String> appServerNames, List<String> appNames, List<String> environmentNames, Boolean onlyLatest) {
        String requestUri = liimaConfiguration.getDeploymentsUrl(deploymentState, appServerNames, appNames, environmentNames, onlyLatest);
        return executeRequestAndReceiveList(requestUri, Deployment[].class);
    }

    @Nullable
    private Deployment onlyDeploymentOrNull(List<Deployment> deployments) {
        if (deployments != null && deployments.size() > 1) {
            LOG.error("Too many matching deployment(s) found. Will use the latest one.");
        }
        if (deployments != null && deployments.size() == 1) {
            return deployments.get(0);
        }
        return null;
    }

    String createDeploymentJson(Deployment deployment) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        if (LOG.isDebugEnabled()) {
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
        }

        try {
            String json = mapper.writeValueAsString(deployment);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Liima new Deployment JSON String:");
                LOG.debug(json);
            }
            return json;

        } catch (JsonProcessingException e) {
            LOG.error("Could not create deployment json structure.", e);
            throw new IllegalArgumentException("could not serialize deployment-object into JSON.");
        }
    }

    @Nullable
    public String getVersion(String applicationName, List<Deployment> deployments) {
        for (Deployment deployInfo : deployments) {
            if (!"success".equals(deployInfo.getState())) {
                LOG.warn("The last deployment of {} was not successful!", deployInfo.getAppServerName());
            }

            for (AppWithVersion appWithVersion : deployInfo.getAppsWithVersion()) {
                LOG.debug("Try to find version for given application. {}:{}", appWithVersion.getApplicationName(), appWithVersion.getVersion());
                if (applicationName.equalsIgnoreCase(appWithVersion.getApplicationName())) {
                    LOG.debug("Found version for {}", applicationName);
                    return appWithVersion.getVersion();
                }
            }
        }
        return null;
    }

    public Deployment sendNewDeployment(Deployment deployment) {
        String json = createDeploymentJson(deployment);

        StringEntity requestEntity;
        requestEntity = new StringEntity(json, "UTF-8");
        requestEntity.setContentType("application/json;charset=UTF-8");

        try {
            HttpResponse httpResponse = Request.Post(liimaConfiguration.getDeploymentsUrl())
                                               .addHeader("content-type", "application/json")
                                               .addHeader("Accept", "application/json")
                                               .body(requestEntity)
                                               .execute()
                                               .returnResponse();

            // retrieve content even if the request failed (Liima includes some details in the response content)
            String jsonString = consumeJsonString(httpResponse);

            StatusLine statusLine = httpResponse.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            String reasonPhrase = statusLine.getReasonPhrase();
            if (statusCode >= 300) {
                LOG.error("Could not send deployment request, Status-Code: {}", statusCode);
                LOG.error("Could not send deployment request, Reason-Phrase: {}", reasonPhrase);
                LOG.error("Error Response Content: {}", jsonString);

                throw new HttpResponseException(statusCode, reasonPhrase);
            }

            LOG.debug("Deploy Request Response:");
            LOG.debug(jsonString);

            return createDTO(jsonString, Deployment.class);

        } catch (IOException e) {
            throw new RuntimeException("Failed to send deployment request.", e);
        }
    }

    public Deployment trackDeploymentRequest(Deployment requestedDeployment) {
        Deployment trackedDeployment;
        long startTime = System.currentTimeMillis();
        boolean timeout;
        boolean makeAnotherQuery;
        do {
            String deploymentUrl = liimaConfiguration.getDeploymentsUrl() + "/" + requestedDeployment.getId();
            try {
                String jsonTrackingResponse = Request.Get(deploymentUrl)
                                                     .addHeader("Accept", "application/json")
                                                     .execute()
                                                     .returnContent()
                                                     .asString();

                LOG.debug("JSON from tracking request:");
                LOG.debug(jsonTrackingResponse);

                trackedDeployment = createDTO(jsonTrackingResponse, Deployment.class);
                LOG.info(format("State of deployment with trackingId [{0,number,#}] is [{1}]", trackedDeployment.getTrackingId(), trackedDeployment.getState()));

                long currentTime = System.currentTimeMillis();
                timeout = ((currentTime - startTime) > maxPollingDurationInMs);

                makeAnotherQuery = (!trackedDeployment.isComplete() && !timeout);

                if (trackedDeployment.isRequested()) {
                    LOG.warn("State of deployment is [requested] which is unexpected. Will stop polling. Please check in Liima.");
                    makeAnotherQuery = false;
                }

                if (trackedDeployment.isScheduled()) {
                    LOG.warn("State of deployment is [scheduled]. Will stop polling. Please check in Liima.");
                    makeAnotherQuery = false;
                }

                if (makeAnotherQuery) {
                    pause();
                }

            } catch (IOException e) {
                LOG.error("Could not poll for deployment request status.", e);
                throw new RuntimeException("Could not poll for deployment status.");
            }
        } while (makeAnotherQuery);

        LOG.info("Stopped tracking deployment. Current state [{}]", trackedDeployment.getState());

        return trackedDeployment;
    }

    void pause() {
        try {
            LOG.debug("will sleep for {} ms", pollingIntervalInMs);
            Thread.sleep(pollingIntervalInMs);
        } catch (InterruptedException ignored) {
            LOG.warn("Could not sleep. Was interrupted. Continue anyway.");
        }
    }

    public String retrieveVersion(String applicationName, String environmentName, String serverName) {
        Deployment deployment = retrieveDeployment(serverName, environmentName);
        return (deployment != null) ? getVersion(applicationName, singletonList(deployment)) : "";
    }

    @Nullable
    public List<Resource> retrieveResources() {
        String requestUri = liimaConfiguration.getResourcesUrl();
        List<Resource> resources = executeRequestAndReceiveList(requestUri, Resource[].class);
        return resources != null ? resources.stream().filter(Objects::nonNull).collect(toList()) : null;
    }

    public <T> List<T> getResourcesOfType(List<Resource> resources, Class<T> type) {
        return resources
                .stream()
                .filter(type::isInstance)
                .map(type::cast)
                .collect(toList());
    }

    @Nullable
    public List<Environment> retrieveEnvironments() {
        String requestUri = liimaConfiguration.getEnvironmentsUrl();
        return executeRequestAndReceiveList(requestUri, Environment[].class);
    }

    @Nullable
    public List<Property> retrieveProperties(String resourceName, String releaseName) {
        String requestUri = liimaConfiguration.getResourcePropertiesUrl(resourceName, releaseName);
        return executeRequestAndReceiveList(requestUri, Property[].class);
    }

    @Nullable
    public List<ResourceRelation> retrieveResourceRelations(String resourceName, String releaseName) {
        String requestUri = liimaConfiguration.getResourceRelationsUrl(resourceName, releaseName);
        return executeRequestAndReceiveList(requestUri, ResourceRelation[].class);
    }

    @Nullable
    public List<Property> getResourceRelationProperties(String resourceName, String releaseName, String relatedResourceName, String relatedReleaseName) {
        String requestUri = liimaConfiguration.getResourceRelationPropertiesUrl(resourceName, releaseName, relatedResourceName, relatedReleaseName);
        return executeRequestAndReceiveList(requestUri, Property[].class);
    }

    @Nullable
    public Release getExactOrClosestPastRelease(String resourceName, String releaseName, String relationType) {
        String requestUri = liimaConfiguration.getClosestPastReleaseUrl(resourceName, releaseName, relationType);
        return executeRequestAndReceiveObject(requestUri, Release.class);
    }

    @Nullable
    private <T> T executeRequestAndReceiveObject(String requestUri, Class<T> responseValueType) {
        HttpGet request = new HttpGet(requestUri);
        request.addHeader("accept", "application/json");
        try (CloseableHttpClient httpClient = createHttpClient()) {
            LOG.debug("GET Request: {}", requestUri);
            HttpResponse response = executeRequest(request, httpClient);
            if (response.getStatusLine().getStatusCode() == 404) {
                LOG.debug("Could not find resource: {}", requestUri);
                return null;
            }
            String jsonString = consumeJsonString(response);
            return createDTO(jsonString, responseValueType);
        } catch (IOException e) {
            LOG.error("Could not execute request to read object: {}", requestUri);
            if (LOG.isDebugEnabled()) {
                LOG.error("IOException: ", e);
            }
            return null;
        }
    }

    @Nullable
    <T> List<T> executeRequestAndReceiveList(String requestUri, Class<T[]> responseValueType) {
        HttpGet request = new HttpGet(requestUri);
        request.addHeader("accept", "application/json");
        try (CloseableHttpClient httpClient = createHttpClient()) {
            LOG.debug("GET Request: {}", requestUri);
            HttpResponse response = executeRequest(request, httpClient);
            if (response.getStatusLine().getStatusCode() == 404) {
                LOG.debug("Could not find resource: {}", requestUri);
                return null;
            }
            String jsonString = consumeJsonString(response);
            return createDTOs(jsonString, responseValueType);
        } catch (IOException e) {
            LOG.error("Could not execute request to read list: {}", requestUri);
            LOG.error(e.getLocalizedMessage());
            if (LOG.isDebugEnabled()) {
                LOG.error("IOException: ", e);
            }
            return null;
        }
    }

    private HttpResponse executeRequest(HttpGet request, CloseableHttpClient httpClient) throws IOException {
        HttpResponse response = httpClient.execute(request);
        LOG.debug("Received Status Code: {}", response.getStatusLine().getStatusCode());
        return response;
    }

    private String consumeJsonString(HttpResponse response) throws IOException {
        HttpEntity responseEntity = response.getEntity();
        InputStream content = responseEntity.getContent();
        String jsonString = IOUtils.toString(content, "UTF-8");
        EntityUtils.consume(responseEntity);
        return jsonString;
    }

    <T> T createDTO(String jsonString, Class<T> valueType) throws IOException {
        LOG.debug("JSON String: {}", jsonString);
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        return mapper.readValue(jsonString, valueType);
    }

    <T> List<T> createDTOs(String jsonString, Class<T[]> valueType) throws IOException {
        LOG.debug("JSON String: {}", jsonString);
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        mapper.disable(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE);
        return Arrays.asList(mapper.readValue(jsonString, valueType));
    }

    public String createDeploymentsWebUri(String serverName, String applicationName, String environment) {
        return liimaConfiguration.createDeploymentsWebUri(serverName, applicationName, environment);
    }

    public String createDeploymentsWebUri(String trackingId) {
        Filters filters = new Filters.Builder()
                .add(FilterName.TRACKING_ID, FilterComparator.EQ, trackingId)
                .build();
        return liimaConfiguration.createDeploymentsWebUri(filters);
    }

}
