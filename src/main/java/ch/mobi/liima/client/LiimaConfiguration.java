package ch.mobi.liima.client;

/*-
 * §
 * Liima Client
 * --
 * Copyright (C) 2017 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.liima.client.dto.DeploymentState;
import com.google.common.net.UrlEscapers;
import org.apache.commons.text.WordUtils;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import static ch.mobi.liima.client.FilterComparator.EQ;
import static ch.mobi.liima.client.FilterName.APPLICATION;
import static ch.mobi.liima.client.FilterName.APPLICATION_SERVER;
import static ch.mobi.liima.client.FilterName.ENVIRONMENT;
import static ch.mobi.liima.client.FilterName.LATEST_ONLY;
import static ch.mobi.liima.client.FilterName.STATE;
import static java.text.MessageFormat.format;
import static java.util.stream.Collectors.toList;

public class LiimaConfiguration {

    private static final String LIIMA_WEB_PATH = "/AMW_web";
    private static final String LIIMA_ANGULAR_PATH = "/AMW_angular";
    private static final String LIIMA_REST_PATH = "/AMW_rest/resources";

    private static final String DEPLOYMENTS_PATH = "/deployments";
    private static final String DEPLOYMENTS_WITH_FILTERS_PATH = "/deployments/filter?filters={0}";
    private static final String RESOURCES_PATH = "/resources";
    private static final String ENVIRONMENTS_PATH = "/environments";
    private static final String RESOURCE_PATH_PATTERN = "/resources/{0}/{1}"; // 0: resource name, {1} release name
    private static final String RESOURCE_PROPERTIES_PATH_PATTERN = RESOURCE_PATH_PATTERN + "/properties";
    private static final String RESOURCE_RELATIONS_PATH_PATTERN = RESOURCE_PATH_PATTERN + "/relations";
    private static final String RESOURCE_RELATION_PROPERTIES_PATH_PATTERN = RESOURCE_PATH_PATTERN + "/relations/{2}/{3}/properties"; // 2: related resource name, {3} related release name

    // links that point to the WebUI:
    /** angularUri + ... */
    private static final String WEB_UI_ANGULAR_DEPLOYMENTS = "/#/deployments?filters={0}";

    /** number of seconds the http client should wait for a response */
    private int timeout = 10;

    /** number of parallel pooled connection for the http client, used for both max connections and connections per route */
    private int maxConnections = 50;

    /**
     * Liima base url pointing to the REST base path with <b>no trailing slash</b>, ex: https://liima/AMW_rest/resources
     */
    private final String baseUrl;

    /**
     * url pointing to the older web interface with <b>no trailing slash</b>, ex: https://liima/AMW_web
     */
    private final String webUrl;

    /**
     * url pointing to the newer angular based web interface with <b>no trailing slash</b>, ex https://liima/AMW_angular
     */
    private final String angularUrl;

    /**
     * create a new Liima configuration based on the rest base url (basePath from /AMW_rest/resources/swagger.json).
     * The web uri and angular uri and then guessed based on the {@link #baseUrl}
     *
     * @param baseUrl the url pointing to the root of the Liima rest resources,
     *                e.g.: https://liima.host.domain/AMW_rest/resources - no trailing slash /
     */
    public LiimaConfiguration(@NotNull String baseUrl) {
        // guessing the urls...
        String webUrl = baseUrl.replaceFirst(LIIMA_REST_PATH, LIIMA_WEB_PATH);
        String angularUrl = baseUrl.replaceFirst(LIIMA_REST_PATH, LIIMA_ANGULAR_PATH);

        this.baseUrl = baseUrl;
        this.webUrl = webUrl;
        this.angularUrl = angularUrl;
    }

    /**
     * Create Liima configuration by specifying the rest base url, the weburl and the angular url in case they are
     * not located under the same host.domain.
     *
     * @param baseUrl the url pointing to the root of the Liima rest resources,
     *                e.g.: https://liima.host.domain/AMW_rest/resources - no trailing slash /
     * @param webUrl the url pointing the the web ui, e.g.: https://liima.host.domain/AMW_web - no trailing slash /
     * @param angularUrl the url pointing to the angular ui, e.g.: https://liima.host.domain/AMW_angular - no
     *                   trailing slash /
     */
    public LiimaConfiguration(@NotNull String baseUrl, @NotNull String webUrl, @NotNull String angularUrl) {
        this.baseUrl = baseUrl;
        this.webUrl = webUrl;
        this.angularUrl = angularUrl;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getResourcesUrl() {
        return baseUrl + RESOURCES_PATH;
    }

    public String getEnvironmentsUrl() {
        return baseUrl + ENVIRONMENTS_PATH;
    }

    public String getDeploymentsUrl() {
        return baseUrl + DEPLOYMENTS_PATH;
    }

    String getDeploymentsUrl(DeploymentState deploymentState, List<String> appServerNames, List<String> appNames, List<String> environmentNames, Boolean onlyLatest) {
        Filters.Builder fb = new Filters.Builder();

        if (deploymentState != null) {
            fb.add(STATE, EQ, deploymentState.getDisplayName());
        }

        fb.add(APPLICATION_SERVER, EQ, appServerNames);
        fb.add(APPLICATION, EQ, appNames);

        // workaround, Liima is very picky with environment names...
        // https://github.com/liimaorg/liima/issues/418
        if (environmentNames != null && environmentNames.size() > 0) {
            List<String> capitalizedEnvNames = environmentNames.stream().map(WordUtils::capitalizeFully).collect(toList());
            fb.add(ENVIRONMENT, EQ, capitalizedEnvNames);
        }

        if (onlyLatest != null) {
            fb.add(LATEST_ONLY);
        }

        String filterString = fb.build().getFilterString();

        return baseUrl + format(DEPLOYMENTS_WITH_FILTERS_PATH, urlEncode(filterString));
    }

    public String getClosestPastReleaseUrl(String resourceName, String releaseName, String relationType) {
        return SimpleUrlBuilder.build(b -> b.path(baseUrl)
                                            .path(RESOURCES_PATH)
                                            .path(urlEncode(resourceName))
                                            .path("lte")
                                            .path(releaseName)
                                            .queryParameter("type", relationType));
    }

    public String getResourcePropertiesUrl(String resourceName, String releaseName) {
        return baseUrl + format(RESOURCE_PROPERTIES_PATH_PATTERN, resourceName, releaseName);
    }

    public String getResourceRelationsUrl(String resourceName, String releaseName) {
        return baseUrl + format(RESOURCE_RELATIONS_PATH_PATTERN, resourceName, releaseName);
    }

    public String getResourceRelationPropertiesUrl(String resourceName, String releaseName, String relatedResourceName, String relatedReleaseName) {
        return baseUrl + format(RESOURCE_RELATION_PROPERTIES_PATH_PATTERN, resourceName, releaseName, relatedResourceName, relatedReleaseName);
    }

    private String urlEncode(String string) {
        if (string == null) {
            return null;
        }
        return UrlEscapers.urlFragmentEscaper().escape(string);
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getMaxConnections() {
        return maxConnections;
    }

    public void setMaxConnections(int maxConnections) {
        this.maxConnections = maxConnections;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public String getAngularUrl() {
        return angularUrl;
    }

    public String createDeploymentsWebUri(String serverName, String applicationName, String environment) {
        // workaround, Liima is very picky with environment names...
        // https://github.com/liimaorg/liima/issues/418
        String envCap = WordUtils.capitalizeFully(environment);

        Filters filters = new Filters.Builder()
                .add(APPLICATION_SERVER, EQ, serverName)
                .add(APPLICATION, EQ, applicationName)
                .add(ENVIRONMENT, EQ, envCap)
                .build();

        return createDeploymentsWebUri(filters);
    }

    public String createDeploymentsWebUri(Filters filters) {
        String filterString = filters.getFilterString();

        return getAngularUrl() + format(WEB_UI_ANGULAR_DEPLOYMENTS, urlEncode(filterString));
    }
}
