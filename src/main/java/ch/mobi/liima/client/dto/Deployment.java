package ch.mobi.liima.client.dto;

/*-
 * §
 * Liima Client
 * --
 * Copyright (C) 2017 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.text.WordUtils;

public class Deployment implements Serializable {

    @JsonProperty("id") private Long id;
    @JsonProperty("trackingId") private Long trackingId;
    @JsonProperty("appServerName") private String appServerName;
    @JsonProperty("state") private String state;
    @JsonProperty("deploymentDate") private Long deploymentDate;
    @JsonProperty("stateToDeploy") private Long stateToDeploy;
    @JsonProperty("releaseName") private String releaseName;
    @JsonProperty("targetPlatform") private String targetPlatform;
    @JsonProperty("requestUser") private String requestUser;
    @JsonProperty("confirmUser") private String confirmUser;
    @JsonProperty("cancelUser") private String cancelUser;
    @JsonProperty("requestOnly") private boolean requestOnly = false;
    @JsonProperty("simulate") private Boolean simulate;
    @JsonProperty("executeShakedownTest") private Boolean executeShakedownTest;
    @JsonProperty("neighbourhoodTest") private Boolean neighbourhoodTest;
    @JsonProperty("sendEmail") private Boolean sendEmail;
    @JsonProperty("environmentName") private String environmentName;

    @JsonProperty("appsWithVersion") private List<AppWithVersion> appsWithVersion;
    @JsonProperty("deploymentParameters") private List<DeploymentParameter> deploymentParameters;

    @JsonCreator
    public Deployment() {
    }

    public Deployment(String appServerName, String environmentName) {
        this.appServerName = appServerName;
        this.environmentName = environmentName;
    }

    public Deployment(String appServerName, String releaseName, String environmentName) {
        this(appServerName, environmentName);
        this.releaseName = releaseName;
    }

    private void initializeAppsWithMvnVersion() {
        if (appsWithVersion == null) {
            appsWithVersion = new ArrayList<>();
        }
    }

    public void addAppWithVersion(AppWithVersion appWithVersion) {
        initializeAppsWithMvnVersion();

        if (appWithVersion != null) {
            appsWithVersion.add(appWithVersion);
        }
    }

    public List<AppWithVersion> getAppsWithVersion() {
        initializeAppsWithMvnVersion();
        return Collections.unmodifiableList(appsWithVersion);
    }

    public void setAppsWithVersion(List<AppWithVersion> appsWithVersion) {
        this.appsWithVersion = appsWithVersion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(Long trackingId) {
        this.trackingId = trackingId;
    }

    public String getAppServerName() {
        return appServerName;
    }

    public void setAppServerName(String appServerName) {
        this.appServerName = appServerName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getDeploymentDate() {
        return deploymentDate;
    }

    public void setDeploymentDate(Long deploymentDate) {
        this.deploymentDate = deploymentDate;
    }

    public Long getStateToDeploy() {
        return stateToDeploy;
    }

    public void setStateToDeploy(Long stateToDeploy) {
        this.stateToDeploy = stateToDeploy;
    }

    public String getReleaseName() {
        return releaseName;
    }

    public void setReleaseName(String releaseName) {
        this.releaseName = releaseName;
    }

    public String getTargetPlatform() {
        return targetPlatform;
    }

    public void setTargetPlatform(String targetPlatform) {
        this.targetPlatform = targetPlatform;
    }

    public String getRequestUser() {
        return requestUser;
    }

    public void setRequestUser(String requestUser) {
        this.requestUser = requestUser;
    }

    public String getConfirmUser() {
        return confirmUser;
    }

    public void setConfirmUser(String confirmUser) {
        this.confirmUser = confirmUser;
    }

    public String getCancelUser() {
        return cancelUser;
    }

    public void setCancelUser(String cancelUser) {
        this.cancelUser = cancelUser;
    }

    public boolean isRequestOnly() {
        return requestOnly;
    }

    public void setRequestOnly(boolean requestOnly) {
        this.requestOnly = requestOnly;
    }

    public Boolean getSimulate() {
        return simulate;
    }

    public void setSimulate(Boolean simulate) {
        this.simulate = simulate;
    }

    public Boolean getExecuteShakedownTest() {
        return executeShakedownTest;
    }

    public void setExecuteShakedownTest(Boolean executeShakedownTest) {
        this.executeShakedownTest = executeShakedownTest;
    }

    public Boolean getNeighbourhoodTest() {
        return neighbourhoodTest;
    }

    public void setNeighbourhoodTest(Boolean neighbourhoodTest) {
        this.neighbourhoodTest = neighbourhoodTest;
    }

    public Boolean getSendEmail() {
        return sendEmail;
    }

    public void setSendEmail(Boolean sendEmail) {
        this.sendEmail = sendEmail;
    }

    public String getEnvironmentName() {
        // make sure the environment name is capitalized when sent to Liima:
        return (environmentName != null) ? WordUtils.capitalizeFully(environmentName) : null;
    }

    public void setEnvironmentName(String environmentName) {
        this.environmentName = environmentName;
    }

    public List<DeploymentParameter> getDeploymentParameters() {
        return deploymentParameters;
    }

    public void setDeploymentParameters(List<DeploymentParameter> deploymentParameters) {
        this.deploymentParameters = deploymentParameters;
    }

    /**
     * @return {@code true} if the state of the given deploymentRequest is either success of failed
     *
     * @see Deployment#state
     */
    @JsonIgnore
    public boolean isComplete() {
        return ("success".equalsIgnoreCase(this.state)
                || "failed".equalsIgnoreCase(this.state)
                || "canceled".equalsIgnoreCase(this.state)
                || "rejected".equalsIgnoreCase(this.state)
                || "scheduled".equalsIgnoreCase(this.state));
    }

    @JsonIgnore
    public boolean hasFailed() {
        return "failed".equalsIgnoreCase(this.state);
    }

    @JsonIgnore
    public boolean isSuccessful() {
        return "success".equalsIgnoreCase(this.state);
    }

    @JsonIgnore
    public boolean isRequested() {
        return "requested".equalsIgnoreCase(this.state);
    }

    @JsonIgnore
    public boolean isScheduled() {
        return "scheduled".equalsIgnoreCase(this.state);
    }

    /**
     * convenience method to read the application name with one application per server
     * @return The first application name read from Liima or {@code null}
     */
    @JsonIgnore
    public String getFirstApplicationName() {
        List<AppWithVersion> appsWithVersions = getAppsWithVersion();
        if (CollectionUtils.isNotEmpty(appsWithVersions)) {
            return appsWithVersions.get(0).getApplicationName();
        }
        return null;
    }

    /**
     * convenience method to read the version number if there is only one application deployed
     * @return The first application version read from Liima or or {@code null}
     */
    @JsonIgnore
    public String getFirstApplicationVersion() {
        List<AppWithVersion> appsWithVersions = getAppsWithVersion();
        if (CollectionUtils.isNotEmpty(appsWithVersions)) {
            return appsWithVersions.get(0).getVersion();
        }
        return null;
    }

    @JsonIgnore
    public void addDeploymentParameter(String key, String value) {
        if (this.deploymentParameters == null) {
            this.deploymentParameters = new ArrayList<>();
        }

        deploymentParameters.add(new DeploymentParameter(key, value));
    }

    @JsonIgnore
    public String getDeploymentParameter(String key) {
        if (this.deploymentParameters != null) {
            for (DeploymentParameter deploymentParameter : this.deploymentParameters) {
                if (deploymentParameter.getKey().equalsIgnoreCase(key)) {
                    return deploymentParameter.getValue();
                }
            }
        }

        return null;
    }
}
