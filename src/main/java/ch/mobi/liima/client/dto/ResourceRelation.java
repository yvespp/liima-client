package ch.mobi.liima.client.dto;

/*-
 * §
 * Liima Client
 * --
 * Copyright (C) 2017 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResourceRelation {

    @JsonProperty
    private String relatedResourceName;
    @JsonProperty
    private String relatedResourceRelease;
    @JsonProperty
    private String relationName;
    @JsonProperty
    private String type;

//    private List<Template> templates;

    public ResourceRelation() {
    }

    public ResourceRelation(String relatedResourceName, String relatedResourceRelease, String relationName, String type) {
        this.relatedResourceName = relatedResourceName;
        this.relatedResourceRelease = relatedResourceRelease;
        this.relationName = relationName;
        this.type = type;
    }

    public String getRelatedResourceName() {
        return relatedResourceName;
    }

    public String getRelatedResourceRelease() {
        return relatedResourceRelease;
    }

    public String getRelationName() {
        return relationName;
    }

    public String getType() {
        return type;
    }
}
