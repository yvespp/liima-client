package ch.mobi.liima.client;

/*-
 * §
 * Liima Client
 * --
 * Copyright (C) 2017 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.liima.client.dto.AppWithVersion;
import ch.mobi.liima.client.dto.Deployment;
import ch.mobi.liima.client.dto.DeploymentParameter;
import ch.mobi.liima.client.dto.DeploymentState;
import com.jayway.jsonpath.JsonPath;
import net.minidev.json.JSONArray;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class LiimaClientTest {

    private LiimaClient client;

    @Before
    public void setUp() throws Exception {
        LiimaConfiguration configuration = new LiimaConfiguration("http://localhost");
        client = new LiimaClient(configuration);
    }

    private String loadJsonStringFromFile(String filename) {
        try(InputStream resourceAsStream = getClass().getResourceAsStream("/" + filename)) {
            return IOUtils.toString(resourceAsStream, "UTF-8");

        } catch (IOException ignored) {
            return null;
        }

    }

    @Test
    public void testDeploymentWithParameters() {
        // arrange
        Deployment deployment = new Deployment("vvn", "RL-16.04", "Y");
        List<DeploymentParameter> deploymentParameters = new ArrayList<>();
        DeploymentParameter deploymentParameter = new DeploymentParameter();
        deploymentParameter.setKey("testKey");
        deploymentParameter.setValue("testValue");
        deploymentParameters.add(deploymentParameter);
        deployment.setDeploymentParameters(deploymentParameters);
        // use convenience method to add another one:
        deployment.addDeploymentParameter("otherKey", "otherValue");

        AppWithVersion vvnApp = new AppWithVersion("ex_app_backend", "1.1.0-220");
        deployment.addAppWithVersion(vvnApp);

        // act
        String json = client.createDeploymentJson(deployment);

        JSONArray deployParams = JsonPath.read(json, "$.deploymentParameters");

        // assert
        assertNotNull(deployParams);
        assertThat(deployParams, hasSize(2));
    }

    @Test
    public void testNewAppsWithVersionsExist() {
        // arrange
        Deployment deployment = new Deployment();
        deployment.setAppServerName("vvn");
        deployment.setReleaseName("RL-16.04");
        deployment.setEnvironmentName("Y");

        AppWithVersion vvnApp = new AppWithVersion("ex_app_backend", "1.1.0-220");
        deployment.addAppWithVersion(vvnApp);

        // act
        String json = client.createDeploymentJson(deployment);

        JSONArray appWithVersion = JsonPath.read(json, "$.appsWithVersion");

        // assert
        assertNotNull(appWithVersion);
        assertThat(appWithVersion, hasSize(1));
    }


    @Test
    public void testCreateDeploymentInfo() throws Exception {
        // arrange
        String jsonString = loadJsonStringFromFile("deployment/example-vvn-deployment-response.json");

        // act
        Deployment deployInfo = client.createDTO(jsonString, Deployment.class);

        // assert
        assertNotNull(deployInfo);
    }

    @Test
    public void testCreateDeploymentRequestSingleApp() {
        // arrange
        Deployment deployment = new Deployment();
        deployment.setAppServerName("vvn");
        deployment.setReleaseName("RL-16.04");
        deployment.setEnvironmentName("Y");

        AppWithVersion vvnApp = new AppWithVersion("ex_app_backend", "1.1.0-220");
        deployment.addAppWithVersion(vvnApp);

        // act
        String json = client.createDeploymentJson(deployment);

        // assert
        assertEquals("vvn", JsonPath.read(json, "$.appServerName"));
        assertEquals("RL-16.04", JsonPath.read(json, "$.releaseName"));
        JSONArray cancelUser = JsonPath.read(json, "$.[?(@.cancleUser)]");
        assertEquals(0, cancelUser.size());

    }


    @Test
    public void testCreateResponseForVvn() throws IOException {
        // arrange
        String jsonString = loadJsonStringFromFile("deployment/example-vvn.json");

        // act
        List<Deployment> response = client.createDTOs(jsonString, Deployment[].class);

        // assert
        assertNotNull(response);
        Assert.assertEquals("received one info", 1, response.size());
    }

    @Test
    public void testCreateResponseForMwf() throws IOException {
        // arrange
        String jsonString = loadJsonStringFromFile("deployment/example-mwf.json");

        // act
        List<Deployment> response = client.createDTOs(jsonString, Deployment[].class);

        // assert
        assertNotNull(response);
        Assert.assertEquals("received one info", 1, response.size());
        Assert.assertEquals("received multiple maven versions", 5, response.get(0).getAppsWithVersion().size());
    }

    @Test
    public void testCreateResponseForPdn() throws IOException {
        // arrange
        String jsonString = loadJsonStringFromFile("deployment/example-vvn-pdn.json");

        // act
        // will query for vvn & pdn
        // but will receive vvn, pdn and pdn_replikator
        List<Deployment> response = client.createDTOs(jsonString, Deployment[].class);

        // assert
        assertNotNull(response);
        Assert.assertEquals("received one info", 3, response.size());
    }

    @Test
    public void testCreateResponseFor2016() throws IOException {
        // arrange
        String jsonString = loadJsonStringFromFile("deployment/example-dez-2015.json");

        // act
        List<Deployment> response = client.createDTOs(jsonString, Deployment[].class);

        // assert
        assertNotNull(response);
        assertThat(response.size(), lessThan(25));
    }

    @Test
    public void testCreateResponseFor2016Query() throws IOException {
        // arrange
        String jsonString = loadJsonStringFromFile("deployment/example-dez-2015-query.json");

        // act
        List<Deployment> response = client.createDTOs(jsonString, Deployment[].class);

        // assert
        assertNotNull(response);
        assertTrue(response.size() > 0);
    }

    @Test
    public void testGetVvnMavenVersion() throws IOException {
        // arrange
        String jsonString = loadJsonStringFromFile("deployment/example-vvn.json");

        // act
        List<Deployment> deployments = client.createDTOs(jsonString, Deployment[].class);

        String version = client.getVersion("ImARandomValue", deployments);

        // assert
        Assert.assertEquals("parsed version matches", "1.1.0-291", version);
    }

    @Test
    public void testPause() {
        // arrange
        LiimaClient client = new LiimaClient(new LiimaConfiguration(""));
        client.setPollingIntervalInMs(100);

        long startTime = System.currentTimeMillis();

        // act
        client.pause();

        // assert
        long stopTime = System.currentTimeMillis();
        assertTrue("took at least...", stopTime - startTime >= client.getPollingIntervalInMs());
    }

    @Test
    public void testPauseWithException() throws Exception {
        // arrange
        LiimaClient client = new LiimaClient(new LiimaConfiguration(""));
        client.setPollingIntervalInMs(100);

        long startTime = System.currentTimeMillis();

        // act
        Thread.currentThread().interrupt();
        client.pause();

        // well... nothing?
        // assert
        long stopTime = System.currentTimeMillis();
        assertTrue("did not wait 10000", stopTime - startTime < client.getPollingIntervalInMs());
    }

    @Test
    public void retrieveDeploymentsShouldCallCorrectRequestUrl() {
        //given
        LiimaClient client = spy(new LiimaClient(new LiimaConfiguration("http://localhost/")));
        doReturn(null).when(client).executeRequestAndReceiveList(anyString(), any());

        //when
        client.retrieveDeployments(DeploymentState.success, asList("server_1", "server_2"), asList("app_a", "app_b"), asList("T", "P"), true);

        //then
        verify(client).executeRequestAndReceiveList(Matchers.startsWith("http://localhost//deployments/filter?filters="), any());
    }

    @Test
    public void retrieveDeploymentsShouldCallCorrectRequestUrlWithoutParameter() {
        //given
        LiimaClient client = spy(new LiimaClient(new LiimaConfiguration("http://localhost/")));
        doReturn(null).when(client).executeRequestAndReceiveList(anyString(), any());

        //when
        client.retrieveDeployments(null, null, null, null, null);

        //then
        verify(client).executeRequestAndReceiveList(Matchers.startsWith("http://localhost//deployments/filter?filters="), any());
    }

    @Test
    public void retrieveDeploymentsWithServerNameShouldCallCorrectRequestUrl() {
        //given
        LiimaClient client = spy(new LiimaClient(new LiimaConfiguration("http://localhost")));
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        doReturn(null).when(client).executeRequestAndReceiveList(argumentCaptor.capture(), any());

        //when
        client.retrieveDeployments("server 1");

        //then
        String requestUrl = argumentCaptor.getValue();
        assertThat(requestUrl, startsWith("http://localhost/deployments/filter?filters="));
        assertThat(requestUrl, containsString("Latest%20deployment%20job%20for%20App%20Server%20and%20Env"));
        assertThat(requestUrl, containsString("Application%20server"));
        assertThat(requestUrl, containsString("server%201"));
    }

    @Test
    public void retrieveDeploymentsWithServerNameAndEnvironmentShouldCallCorrectRequestUrl() {
        //given
        LiimaClient client = spy(new LiimaClient(new LiimaConfiguration("http://localhost")));
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        doReturn(null).when(client).executeRequestAndReceiveList(argumentCaptor.capture(), any());

        //when
        client.retrieveDeployment("server1", "TTTT");

        //then
        String requestUrl = argumentCaptor.getValue();
        assertThat(requestUrl, startsWith("http://localhost/deployments/filter?filters="));
        assertThat(requestUrl, containsString("Latest%20deployment%20job%20for%20App%20Server%20and%20Env"));
        assertThat(requestUrl, containsString("server1"));
        assertThat(requestUrl, containsString("Tttt"));
    }

    @Test
    public void retrieveDeploymentWebUriWithTrackingId() {
        //given
        LiimaClient client = spy(new LiimaClient(new LiimaConfiguration("http://localhost")));

        //when
        String webUri = client.createDeploymentsWebUri("123456");

        //then
        assertThat(webUri, containsString("Tracking%20Id"));
        assertThat(webUri, containsString("123456"));
    }

}
