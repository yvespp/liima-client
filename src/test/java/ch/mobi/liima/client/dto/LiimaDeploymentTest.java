package ch.mobi.liima.client.dto;

/*-
 * §
 * Liima Client
 * --
 * Copyright (C) 2017 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

public class LiimaDeploymentTest {

    private Deployment deployment;


    @Before
    public void setupDeployment() {
        // arrange
        deployment = new Deployment();
        deployment.setAppServerName("test_server_name");
        List<AppWithVersion> appsWithVersion = new ArrayList<>();
        AppWithVersion appWithVersion1 = new AppWithVersion("test_app_name_1", "1");
        appsWithVersion.add(appWithVersion1);
        AppWithVersion appWithVersion2 = new AppWithVersion("test_app_name_2", "2");
        appsWithVersion.add(appWithVersion2);

        deployment.addDeploymentParameter("param1", "value1");
        deployment.addDeploymentParameter("param2", "value2");

        deployment.setAppsWithVersion(appsWithVersion);
    }

    @Test
    public void getFirstApplicationName() throws Exception {
        // act
        String firstApplicationName = deployment.getFirstApplicationName();

        // assert
        assertThat(firstApplicationName, equalTo("test_app_name_1"));
    }


    @Test
    public void getFirstApplicationVersion() throws Exception {
        // act
        String version = deployment.getFirstApplicationVersion();

        // assert
        assertThat(version, equalTo("1"));
    }

    @Test
    public void getGetDeploymentParameter() throws Exception {
        // assert
        assertThat(deployment.getDeploymentParameter("param1"), equalTo("value1"));
        assertThat(deployment.getDeploymentParameter("parAm1"), equalTo("value1"));
        assertThat(deployment.getDeploymentParameter("param2"), equalTo("value2"));
        assertThat(deployment.getDeploymentParameter("ParAm2"), equalTo("value2"));
    }

}
