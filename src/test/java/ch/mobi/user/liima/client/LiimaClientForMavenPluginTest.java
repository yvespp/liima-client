package ch.mobi.user.liima.client;

/*-
 * §
 * Liima Client
 * --
 * Copyright (C) 2017 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.liima.client.LiimaClient;
import ch.mobi.liima.client.LiimaConfiguration;
import org.junit.Test;

import static java.util.Collections.emptyList;

public class LiimaClientForMavenPluginTest {

    @Test
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void methodsShouldBeAccessibleForMavenPlugin() {
        //given
        LiimaClient client = new LiimaClient(new LiimaConfiguration(""));

        //when
        client.setPollingIntervalInMs(1L);
        client.getPollingIntervalInMs();
        client.setMaxPollingDurationInMs(1L);
        client.getMaxPollingDurationInMs();
        client.getVersion("app", emptyList());
    }
}
