package ch.mobi.user.liima.client;

/*-
 * §
 * Liima Client
 * --
 * Copyright (C) 2017 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.liima.client.LiimaClient;
import ch.mobi.liima.client.LiimaConfiguration;
import ch.mobi.liima.client.dto.Deployment;
import ch.mobi.liima.client.dto.AppWithVersion;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class LiimaClientHttpMockTest extends AbstractLocalLiimaTestBase {

    @Test
    public void testRetrieveVersion() throws Exception {
        // arrange
        this.serverBootstrap.registerHandler("*", new FakeLiimaResponseHandler("deployment/example-dez-2015-query.json"));
        startServer();

        LiimaConfiguration conf = new LiimaConfiguration(baseURL);
        LiimaClient client = new LiimaClient(conf);
        client.setMaxPollingDurationInMs(3000);
        client.setPollingIntervalInMs(600);

        // act
        String applicationName = "ImARandomValue";
        String environmentName = "y";
        String serverName = "vvn";
        String version = client.retrieveVersion(applicationName, environmentName, serverName);

        // assert
        assertEquals("1.1.0-262", version);
    }

    @Test
    public void testTrackDeploymentRequest() throws Exception {
        // arrange

        Deployment requestedDeployment = new Deployment();
        requestedDeployment.setId(123456L);

        this.serverBootstrap.registerHandler("*/123456", new FakeTrackDeploymentHandler());
        startServer();

        LiimaConfiguration conf = new LiimaConfiguration(baseURL);
        LiimaClient client = new LiimaClient(conf);
        client.setMaxPollingDurationInMs(3000);
        client.setPollingIntervalInMs(600);

        // act
        Deployment finalDeployment = client.trackDeploymentRequest(requestedDeployment);

        // assert
        assertNotNull(finalDeployment);
        assertEquals("success", finalDeployment.getState());
    }

    @Test
    public void testTrackDeploymentRequestIntoTimeout() throws Exception {
        // arrange
        Deployment requestedDeployment = new Deployment();
        requestedDeployment.setId(123456L);

        this.serverBootstrap.registerHandler("*/123456", new FakeTrackProgressDeploymentHandler());
        startServer();

        LiimaConfiguration conf = new LiimaConfiguration(baseURL);
        LiimaClient client = new LiimaClient(conf);
        client.setMaxPollingDurationInMs(3000);
        client.setPollingIntervalInMs(600);

        // act
        Deployment trackDeploymentRequest = client.trackDeploymentRequest(requestedDeployment);

        // assert
        assertThat(trackDeploymentRequest.getState(), equalTo("progress"));
    }

    @Test
    public void testSendDeployment() throws Exception {
        // arrange
        Deployment requestedDeployment = new Deployment();
        AppWithVersion appWithVersion = new AppWithVersion("ImARandomValue", "1.20.0-101");
        requestedDeployment.addAppWithVersion(appWithVersion);

        this.serverBootstrap.registerHandler("*", new FakeLiimaResponseHandler("deployment/deploy-request-response.json"));
        startServer();

        LiimaConfiguration conf = new LiimaConfiguration(baseURL);
        LiimaClient client = new LiimaClient(conf);
        client.setMaxPollingDurationInMs(3000);
        client.setPollingIntervalInMs(600);

        // act
        Deployment finalDeployment = client.sendNewDeployment(requestedDeployment);

        // assert
        assertNotNull(finalDeployment);
        assertEquals("scheduled", finalDeployment.getState());
    }

    @Test
    public void testSendBadDeployment() throws Exception {
        // arrange
        Deployment requestedDeployment = new Deployment();
        AppWithVersion appWithVersion = new AppWithVersion("ex_app_vvn_informationssicht_vertrag", "0.1.0-17");
        requestedDeployment.addAppWithVersion(appWithVersion);

        this.serverBootstrap.registerHandler("*", new FakeErrorResponseHandler("deployment/deploy-bad-request-response.json"));
        startServer();

        LiimaConfiguration conf = new LiimaConfiguration(baseURL);
        LiimaClient client = new LiimaClient(conf);
        client.setMaxPollingDurationInMs(3000);
        client.setPollingIntervalInMs(600);

        ByteArrayOutputStream outStream = setupOutputLog();

        // act
        try {
            client.sendNewDeployment(requestedDeployment);
        } catch (Exception ignored) {
        }

        // assert
        String logMessages = outStream.toString("UTF-8");
        assertTrue(logMessages.contains("Status-Code: 400"));
        assertTrue(logMessages.contains("Error Response Content:"));
    }

    @Test
    public void testNoDeploymentRequestWhenFailed() throws Exception {
        // arrange
        Deployment deployment = new Deployment();
        AppWithVersion appWithVersion = new AppWithVersion("ImARandomValue", "1.20.0-889");
        deployment.addAppWithVersion(appWithVersion);

        FakeLiimaResponseSingleDeployHandler handler = new FakeLiimaResponseSingleDeployHandler("deployment/deployment-response-failed.json", "deployment/tracking-deployment-scheduled.json");

        this.serverBootstrap.registerHandler("*", handler);
        startServer();

        LiimaConfiguration conf = new LiimaConfiguration(baseURL);
        LiimaClient client = new LiimaClient(conf);
        client.setMaxPollingDurationInMs(6000);
        client.setPollingIntervalInMs(200);

//        plugin.version = "1.20.0-889";
//        plugin.serverName = "vvn";
//        plugin.targetEnv = "C";
//        plugin.sourceEnv = "Y";

        // act
        Deployment requestedDeployment = client.sendNewDeployment(deployment);
        Deployment trackedRequest = client.trackDeploymentRequest(requestedDeployment);

        // assert
        assertThat(trackedRequest, is(not(nullValue())));
        assertEquals(4, handler.getGetRequestCount());
        assertEquals(1, handler.getPostRequestCount());
        assertEquals(0, handler.getOtherRequestCount());

    }

    @Test
    public void testScheduledDeploymentStopsPolling() throws Exception {
        // arrange
        Deployment deployment = new Deployment();
        AppWithVersion appWithVersion = new AppWithVersion("ImARandomValue", "1.20.0-889");
        deployment.addAppWithVersion(appWithVersion);

        FakeLiimaResponseHandler handler = new FakeLiimaResponseHandler("deployment/tracking-deployment-scheduled-for-date.json");

        this.serverBootstrap.registerHandler("*", handler);
        startServer();

        LiimaConfiguration conf = new LiimaConfiguration(baseURL);
        LiimaClient client = new LiimaClient(conf);
        client.setMaxPollingDurationInMs(6000);
        client.setPollingIntervalInMs(200);

        // act
        Deployment requestedDeployment = client.sendNewDeployment(deployment);
        Deployment trackedRequest = client.trackDeploymentRequest(requestedDeployment);

        // assert
        assertThat(trackedRequest, is(not(nullValue())));
        assertEquals(1, handler.getGetRequestCount());
        assertEquals(1, handler.getPostRequestCount());
        assertEquals(0, handler.getOtherRequestCount());

    }

    @Test
    public void testStopPollingWhenDeploymentNotAllowed() throws Exception {
        // arrange
        Deployment requestedDeployment = new Deployment();
        AppWithVersion appWithVersion = new AppWithVersion("ImARandomValue", "1.20.0-889");
        requestedDeployment.addAppWithVersion(appWithVersion);

        FakeLiimaResponseHandler handler = new FakeLiimaResponseHandler("deployment/deploy-request-response-requested.json");
        this.serverBootstrap.registerHandler("*", handler);
        startServer();

        LiimaConfiguration conf = new LiimaConfiguration(baseURL);
        LiimaClient client = new LiimaClient(conf);
        client.setMaxPollingDurationInMs(3000);
        client.setPollingIntervalInMs(600);

        ByteArrayOutputStream outStream = setupOutputLog();

        // act
        client.trackDeploymentRequest(requestedDeployment);

        // assert
        String logMessages = outStream.toString("UTF-8");
        assertTrue(logMessages.contains("State of deployment is [requested] which is unexpected. Will stop polling. Please check in Liima."));
    }

    @NotNull
    private ByteArrayOutputStream setupOutputLog() {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        PrintStream logPrintStream = new PrintStream(outStream);
        System.setOut(logPrintStream);
        System.setErr(logPrintStream);
        return outStream;
    }

    private String readVersionForTestCase(String jsonExampleFilename, String applicationName, String serverName, String environment) throws Exception {
        // arrange
        this.serverBootstrap.registerHandler("*", new FakeLiimaResponseHandler(jsonExampleFilename));
        startServer();

        LiimaConfiguration conf = new LiimaConfiguration(baseURL);
        LiimaClient client = new LiimaClient(conf);
        client.setMaxPollingDurationInMs(3000);
        client.setPollingIntervalInMs(600);

        // act
        return client.retrieveVersion(applicationName, environment, serverName);
    }

    @Test
    public void testRetrieveDeploymentWithCommonServerName() throws Exception {
        // arrange
        // act
        String version = readVersionForTestCase("deployment/example-vvn-response.json", "ImARandomValue", "vvn", "Y");

        // assert
        assertNotNull(version);
        assertEquals("1.20.0-851", version);
    }

    @Test
    public void testRetrieveDeploymentMulti() throws Exception {
        // arrange
        // act
        String version = readVersionForTestCase("deployment/example-mwf-multi-deployment.json", "ex_mwf", "mwf", "Y");

        // assert
        assertNotNull(version);
        assertEquals("1.20.0-465", version);
    }

    @Test
    public void testRetrieveDeploymentWithException() throws Exception {
        // arrange
        this.serverBootstrap.registerHandler("*", new FakeLiimaResponseHandler("missing-file.json"));
        startServer();

        LiimaConfiguration conf = new LiimaConfiguration(baseURL);
        LiimaClient client = new LiimaClient(conf);
        client.setMaxPollingDurationInMs(3000);
        client.setPollingIntervalInMs(600);

        // act
        Deployment deployment = client.retrieveDeployment("vvn", "Y");

        // assert
        assertNull(deployment);
    }

    @Test
    public void testTryToRetrieveVersionWithWrongApplication() throws Exception {
        // arrange
        this.serverBootstrap.registerHandler("*", new FakeLiimaResponseHandler("deployment/example-dez-2015-query.json"));
        startServer();

        LiimaConfiguration conf = new LiimaConfiguration(baseURL);
        LiimaClient client = new LiimaClient(conf);
        client.setMaxPollingDurationInMs(3000);
        client.setPollingIntervalInMs(600);


        // act
        String applicationName = "ex_app_vvn_backend_error";
        String environmentName = "y";
        String serverName = "vvn";
        String version = client.retrieveVersion(applicationName, environmentName, serverName);

        // assert
        assertNull(version);
    }

}
