package ch.mobi.user.liima.client;

/*-
 * §
 * Liima Client
 * --
 * Copyright (C) 2017 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.liima.client.LiimaClient;
import ch.mobi.liima.client.LiimaConfiguration;
import ch.mobi.liima.client.dto.Application;
import ch.mobi.liima.client.dto.Deployment;
import ch.mobi.liima.client.dto.Environment;
import ch.mobi.liima.client.dto.Property;
import ch.mobi.liima.client.dto.Release;
import ch.mobi.liima.client.dto.Resource;
import ch.mobi.liima.client.dto.ResourceRelation;
import ch.mobi.liima.client.dto.Server;
import org.apache.commons.io.IOUtils;
import org.apache.http.ProtocolVersion;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicStatusLine;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class UserLiimaClientTest {

    private BasicHttpEntity httpEntity;
    private CloseableHttpResponse httpResponseMock;
    private CloseableHttpClient httpClientMock;

    private final String serverName = "liimaJunitServer";
    private final String environment = "X";

    private LiimaConfiguration configuration = new LiimaConfiguration("http://localhost/liima/resources/deployments");
    private LiimaClient liimaClient = new LiimaClient(configuration);
    private LiimaClient clientSpy = spy(liimaClient);


    @Before
    public void setup() throws IOException {
        // arrange
        httpClientMock = mock(CloseableHttpClient.class);
        when(clientSpy.createHttpClient()).thenReturn(httpClientMock);

        httpResponseMock = mock(CloseableHttpResponse.class);
        when(httpClientMock.execute(any(HttpGet.class))).thenReturn(httpResponseMock);

        StatusLine statusLine = new BasicStatusLine(new ProtocolVersion("http", 1, 1), 200, "OK");
        when(httpResponseMock.getStatusLine()).thenReturn(statusLine);

        httpEntity = new BasicHttpEntity();
    }

    @Test
    public void retrieveDeployment() throws Exception {
        InputStream contentInputStream = getClass().getResourceAsStream("/deployment/env-t_server-vvn.json");
        httpEntity.setContent(contentInputStream);
        when(httpResponseMock.getEntity()).thenReturn(httpEntity);

        // act
        Deployment deployment = clientSpy.retrieveDeployment(serverName, environment);

        // assert
        verify(httpClientMock, times(1)).execute(any(HttpGet.class));
        verify(httpResponseMock, atLeast(1)).getStatusLine();

        assertThat(deployment, is(not(nullValue())));
        assertThat(deployment.getAppsWithVersion(), is(not(empty())));
    }

    @Test
    public void retrieveTooManyDeployments() throws Exception {
        InputStream contentInputStream = getClass().getResourceAsStream("/deployment/env-t_server-vvn-star.json");
        httpEntity.setContent(contentInputStream);
        when(httpResponseMock.getEntity()).thenReturn(httpEntity);

        // act
        Deployment deployment = clientSpy.retrieveDeployment(serverName, environment);

        // assert
        verify(httpClientMock, times(1)).execute(any(HttpGet.class));
        verify(httpResponseMock, atLeast(1)).getStatusLine();

        assertThat(deployment, is(nullValue()));
    }

    @Test
    public void retrieveDeploymentsShouldReturnDeploymentsObjects() throws Exception {
        //given
        InputStream contentInputStream = getClass().getResourceAsStream("/deployment/env-t_server-vvn-star.json");
        httpEntity.setContent(contentInputStream);
        when(httpResponseMock.getEntity()).thenReturn(httpEntity);

        //when
        List<Deployment> deployments = clientSpy.retrieveDeployments(null, null, null, null, null);

        //then
        verify(httpClientMock).execute(any(HttpGet.class));
        verify(httpResponseMock, atLeast(1)).getStatusLine();
        assertThat(deployments, is(not(nullValue())));
        assertThat(deployments, hasSize(26));
    }

    @Test
    public void retrieveDeploymentWithErrorContent() throws Exception {
        InputStream contentInputStream = IOUtils.toInputStream("", "UTF-8");
        httpEntity.setContent(contentInputStream);
        when(httpResponseMock.getEntity()).thenReturn(httpEntity);

        // act
        Deployment deployment = clientSpy.retrieveDeployment(serverName, environment);

        // assert
        verify(httpClientMock, times(1)).execute(any(HttpGet.class));
        verify(httpResponseMock, atLeast(1)).getStatusLine();

        assertThat(deployment, is(nullValue()));
    }

    @Test
    public void retrieveDeployments() throws IOException {
        InputStream contentInputStream = getClass().getResourceAsStream("/deployment/env-t_server-vvn-star.json");
        httpEntity.setContent(contentInputStream);
        when(httpResponseMock.getEntity()).thenReturn(httpEntity);

        // act
        List<Deployment> deployments = clientSpy.retrieveDeployments(serverName);

        // assert
        verify(httpClientMock, times(1)).execute(any(HttpGet.class));
        verify(httpResponseMock, atLeast(1)).getStatusLine();

        assertThat(deployments, is(not(nullValue())));
        assertThat(deployments, hasSize(26));
    }

    @Test
    public void retrieveDeploymentsWithError() throws IOException {
        InputStream contentInputStream = IOUtils.toInputStream("", "UTF-8");
        httpEntity.setContent(contentInputStream);
        when(httpResponseMock.getEntity()).thenReturn(httpEntity);

        // act
        List<Deployment> deployments = clientSpy.retrieveDeployments(serverName);

        // assert
        verify(httpClientMock, times(1)).execute(any(HttpGet.class));
        verify(httpResponseMock, atLeast(1)).getStatusLine();

        assertThat(deployments, is(nullValue()));
    }

    @Test
    public void retrieveResources() throws IOException {
        InputStream contentInputStream = getClass().getResourceAsStream("/resources-resources-response.json");
        httpEntity.setContent(contentInputStream);
        when(httpResponseMock.getEntity()).thenReturn(httpEntity);

        // act
        List<Resource> resources = clientSpy.retrieveResources();

        // assert
        verify(httpClientMock, times(1)).execute(any(HttpGet.class));
        verify(httpResponseMock, atLeast(1)).getStatusLine();

        assertThat(resources, is(not(nullValue())));
        assertThat(resources, hasSize(2385));
    }

    @Test
    public void retrieveApplicationResources() throws IOException {
        InputStream contentInputStream = getClass().getResourceAsStream("/resources-resources-response.json");
        httpEntity.setContent(contentInputStream);
        when(httpResponseMock.getEntity()).thenReturn(httpEntity);
        List<Resource> resources = clientSpy.retrieveResources();
        for (Resource resource : resources) {
            assertThat(resource, is(not(nullValue())));
        }

        // act
        List<Application> applications = clientSpy.getResourcesOfType(resources, Application.class);
        List<Server> servers = clientSpy.getResourcesOfType(resources, Server.class);

        // assert
        assertThat(resources, is(not(nullValue())));
        assertThat(resources, hasSize(2385));
        assertThat(applications, is(not(nullValue())));
        assertThat(servers, is(not(nullValue())));
        assertThat(applications, hasSize(1717));
        assertThat(servers, hasSize(668));
    }

    @Test
    public void retrieveEnvironments() throws IOException {
        InputStream contentInputStream = getClass().getResourceAsStream("/deployment/environments.json");
        httpEntity.setContent(contentInputStream);
        when(httpResponseMock.getEntity()).thenReturn(httpEntity);

        // act
        List<Environment> environments = clientSpy.retrieveEnvironments();

        // assert
        verify(httpClientMock, times(1)).execute(any(HttpGet.class));
        verify(httpResponseMock, atLeast(1)).getStatusLine();

        assertThat(environments, is(not(nullValue())));
        assertThat(environments, hasSize(17));
    }

    @Test
    public void retrieveProperties() throws IOException {
        InputStream contentInputStream = getClass().getResourceAsStream("/properties/resources-properties-1.json");
        httpEntity.setContent(contentInputStream);
        when(httpResponseMock.getEntity()).thenReturn(httpEntity);

        // act
        List<Property> properties = clientSpy.retrieveProperties("", "");

        // assert
        verify(httpClientMock, times(1)).execute(any(HttpGet.class));
        verify(httpResponseMock, atLeast(1)).getStatusLine();

        assertThat(properties, is(not(nullValue())));
        assertThat(properties, hasSize(15));
    }

    @Test
    public void retrieveResourceRelations() throws IOException {
        InputStream contentInputStream = getClass().getResourceAsStream("/resources/resource-relations.json");
        httpEntity.setContent(contentInputStream);
        when(httpResponseMock.getEntity()).thenReturn(httpEntity);

        // act
        List<ResourceRelation> resources = clientSpy.retrieveResourceRelations("", "");

        // assert
        verify(httpClientMock, times(1)).execute(any(HttpGet.class));
        verify(httpResponseMock, atLeast(1)).getStatusLine();

        assertThat(resources, is(not(nullValue())));
        assertThat(resources, hasSize(4));
    }

    @Test
    public void getResourceRelationPropertiesShouldReturnPropertiesObjects() throws IOException {
        InputStream contentInputStream = getClass().getResourceAsStream("/properties/resource-relation-properties.json");
        httpEntity.setContent(contentInputStream);
        when(httpResponseMock.getEntity()).thenReturn(httpEntity);

        // act
        List<Property> properties = clientSpy.getResourceRelationProperties("", "", "", "");

        // assert
        verify(httpClientMock, times(1)).execute(any(HttpGet.class));
        verify(httpResponseMock, atLeast(1)).getStatusLine();

        assertThat(properties, is(not(nullValue())));
        assertThat(properties, hasSize(3));
        assertThat(properties.stream().anyMatch(p -> "agileRelease".equals(p.getName()) && "true".equals(p.getValue())), is(true));
    }

    @Test
    public void getExactOrClosestPastReleaseShouldReturnAReleaseObject() throws IOException {
        InputStream contentInputStream = getClass().getResourceAsStream("/resources/closest-past-release-ch_mobi_vvn_angebot.json");
        httpEntity.setContent(contentInputStream);
        when(httpResponseMock.getEntity()).thenReturn(httpEntity);

        // act
        Release release = clientSpy.getExactOrClosestPastRelease("", "", null);

        // assert
        verify(httpClientMock).execute(any(HttpGet.class));
        verify(httpResponseMock, atLeast(1)).getStatusLine();

        assertThat(release, is(not(nullValue())));
        assertThat(release.getId(), is(202L));
        assertThat(release.getRelease(), is("RL-16.10"));
        assertThat(release.getRelations(), hasSize(5));
        assertThat(release.getRelations().stream().anyMatch(r -> "FeatureTeam".equals(r.getType()) && "g-one".equals(r.getRelatedResourceName())), is(true));
    }
}
